<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index()
    {
        $this->load->view('head');
        
        $this->load->view('inicio');
        
    }
     public function inicio()
    {
        $this->load->view('head');

        $this->load->view('noticia');
        
    }
    public function usuario()
    {
        $this->load->view('head');
        $data['usuario']=$this->model_usuario->retornarUsuario();
        $this->load->view('CrudUsuario/usuarioo',$data);   
       
    }
     public function modificar1()
    {
        $idUsuario=$_POST['idUsuario'];
        $data['usuario']=$this->model_usuario->recuperarUsuario($idUsuario);
        $this->load->view('head');
        $this->load->view('CrudUsuario/modificarusuarioform',$data);
        // $this->load->view('footer');
    }
    public function modificar1db()
    {
        $idUsuario=$_POST['idUsuario'];
        $primerApellido=$_POST['primerApellido'];
        $data['primerApellido']=$primerApellido; 
         $segundoApellido=$_POST['segundoApellido'];
        $data['segundoApellido']=$segundoApellido; 
         $nombre=$_POST['nombre'];
        $data['nombre']=$nombre; 

        $ci=$_POST['ci'];
        $data['ci']=$ci;

        $Telefono=$_POST['Telefono'];
        $data['Telefono']=$Telefono;
        $Direccion=$_POST['Direccion'];
        $data['Direccion']=$Direccion;
        $Rol=$_POST['Rol'];
        $data['Rol']=$Rol;    
        $nombreUsuario=$_POST['nombreUsuario'];
        $data['nombreUsuario']=$nombreUsuario;
                   
        $this->model_usuario->modificarUsuario($idUsuario,$data);
        $this->load->view('head');
        $this->load->view('CrudUsuario/modificarusuariomensaje',$data);
        // $this->load->view('footer');
    }

    public function agregar1()
    {
        $this->load->view('head');
        $this->load->view('CrudUsuario/agregarform');
        //$this->load->view('footer');    
    }

    public function agregar1db()
    {
        $primerApellido=$_POST['primerApellido'];
        $data['primerApellido']=$primerApellido;  
        $segundoApellido=$_POST['segundoApellido'];
        $data['segundoApellido']=$segundoApellido; 
        $nombre=$_POST['nombre'];
        $data['nombre']=$nombre; 
        $ci=$_POST['ci'];
        $data['ci']=$ci;

        $Telefono=$_POST['Telefono'];
        $data['Telefono']=$Telefono;
        $Direccion=$_POST['Direccion'];
        $data['Direccion']=$Direccion;
        $Rol=$_POST['Rol'];
        $data['Rol']=$Rol;
        
        
        $a=substr($primerApellido,0,1);
        $n=substr($nombre,0,1);
        $c=substr($ci,0,3);

        $Password=md5($a.$n.$c);
        $data['Password']=$Password;

        $a=substr($primerApellido,0,1);
        $n=substr($nombre,0,1);
        $c=substr($ci,0,3);

        $nombreUsuario=($a.$n.$c);
        $data['nombreUsuario']=$nombreUsuario;
            
        $this->model_usuario->agregarUsuario($data);
        $this->load->view('head');
        $this->load->view('CrudUsuario/agregarmensaje',$data);
        //$this->load->view('footer');
    }

    public function eliminar1db()
    {
        $idUsuario=$_POST['idUsuario'];
        $primerApellido=$_POST['primerApellido'];
        $data['primerApellido']=$primerApellido;
        $segundoApellido=$_POST['segundoApellido'];
        $data['segundoApellido']=$segundoApellido;
        $nombre=$_POST['nombre'];
        $data['nombre']=$nombre;
        $ci=$_POST['ci'];
        $data['ci']=$ci;
        $Telefono=$_POST['Telefono'];
        $data['Telefono']=$Telefono;
        $Direccion=$_POST['Direccion'];
        $data['Direccion']=$Direccion;
        $Rol=$_POST['Rol'];
        $data['Rol']=$Rol;
        $nombreUsuario=$_POST['nombreUsuario'];
        $data['nombreUsuario']=$nombreUsuario;
        $Password=$_POST['Password'];
        $data['Password']=$Password;       
        $this->model_usuario->eliminarUsuario($idUsuario);
        $this->load->view('head');
        $this->load->view('CrudUsuario/eliminarmensaje',$data);
        //$this->load->view('footer');
    }

     public function clientes()
    {
        $this->load->view('head');
        $data['cliente']=$this->model_cliente->retornarCliente();
        $this->load->view('CrudCliente/cliente',$data);        
        
   }
    public function listaclientespdf()
    {
        $this->load->Library('pdf');
        $lista=$this->model_cliente->retornarCliente();
        $lista=$lista->result();
        $this->pdf=new Pdf();
        $this->pdf->AddPage();
        $this->pdf->AliasNbPages();
        $this->pdf->SetTitle("ci");
        $this->pdf->SetLeftMargin(15);
        $this->pdf->SetRightMargin(15);
        $this->pdf->SetFillColor(210,210,210);
        $this->pdf->SetFont('Arial','B',11);
        $this->pdf->Cell(120,10,'LISTA DE CLIENTES',0,'C',0);
        $this->pdf->Ln(10);

        foreach ($lista as $row) {
            $ci=$row->ci;
            $this->pdf->Cell(60,10,$ci,'TBLR',0,'L',0);
            $this->pdf->Ln(10);
        }
        $this->pdf->Output('cliente.pdf','I');

    }

        public function lista()
    {
        $this->load->library('pdf');
         $lista=$this->model_cliente->retornarCliente();
        $lista=$lista->result();
        $this->pdf=new pdf();
        $this->pdf->AddPage();
        $this->pdf->SetFont('Arial','B',9);
        $this->pdf->Cell(60,10,'lista de capital');
        $this->pdf->Ln(10);

        foreach ($lista as $row ) {
            $ci=$row->ci;
            $this->pdf->Cell(60,10,$ci);
            $this->pdf->Ln(10);
        }
      $this->pdf->Output('listaCiudades.pdf','I');
    }
    public function subirarchivo()
    {
    
        $this->load->view('subirform');  
    }

    public function subir()
    {
        $config['upload_path']='./uploads';

        $config['allowed_types']='gif|jpg|png';
        $this->load->library('upload',$config);

        if(!$this->upload->do_upload())
        {
            $data['error']=$this->upload->display_errors();
        }
        else
        {
            $metadatos=$this->upload->data();
            $data['img']=$metadatos['file_name'];
        }
        
        $this->load->view('subirform',$data);
      
    }



   public function modificar()
    {
        $idcliente=$_POST['idcliente'];
        $data['cliente']=$this->model_cliente->recuperarCliente($idcliente);
        $this->load->view('head');
        $this->load->view('CrudCliente/modificarclienteform',$data);
        // $this->load->view('footer');

        // eso no es ahi cierto

    }
    public function modificardb()
    {
        $idcliente=$_POST['idcliente'];
        $ci=$_POST['ci'];
        $data['ci']=$ci;
        $primerApellido=$_POST['primerApellido'];
        $data['primerApellido']=$primerApellido;
        $segundoApellido=$_POST['segundoApellido'];
        $data['segundoApellido']=$segundoApellido;  
        $nombres=$_POST['nombres'];
        $data['nombres']=$nombres;
        $sexo=$_POST['sexo'];
        $data['sexo']=$sexo;
        $fechaNacimiento=$_POST['fechaNacimiento'];
        $data['fechaNacimiento']=$fechaNacimiento;
        $direccion=$_POST['direccion'];
        $data['direccion']=$direccion;      
        $this->model_cliente->modificarCliente($idcliente,$data);
        $this->load->view('head');
        $this->load->view('CrudCliente/modificarclientemensaje',$data);
        // $this->load->view('footer');
    }

    public function agregar()
    {
        $this->load->view('head');
        $this->load->view('CrudCliente/agregarform');
        //$this->load->view('footer');

        // en la vista no tienes el archivo  footer por esote mandara un error crealo
    }

    public function agregardb()
    {

        $ci=$_POST['ci'];
        $data['ci']=$ci;
        $primerApellido=$_POST['primerApellido'];
        $data['primerApellido']=$primerApellido;
        $segundoApellido=$_POST['segundoApellido'];
        $data['segundoApellido']=$segundoApellido;  
        $nombres=$_POST['nombres'];
        $data['nombres']=$nombres;
        $sexo=$_POST['sexo'];
        $data['sexo']=$sexo;
        $fechaNacimiento=$_POST['fechaNacimiento'];
        $data['fechaNacimiento']=$fechaNacimiento;
        $direccion=$_POST['direccion'];
        $data['direccion']=$direccion;  
            
        $this->model_cliente->agregarCliente($data);
        $this->load->view('head');
        $this->load->view('CrudCliente/agregarmensaje',$data);
        //$this->load->view('footer');
    }

    public function eliminardb()
    {
        $idcliente=$_POST['idcliente'];
        $ci=$_POST['ci'];
        $data['ci']=$ci;
        $primerApellido=$_POST['primerApellido'];
        $data['primerApellido']=$primerApellido;
        $segundoApellido=$_POST['segundoApellido'];
        $data['segundoApellido']=$segundoApellido;  
        $nombres=$_POST['nombres'];
        $data['nombres']=$nombres;
        $sexo=$_POST['sexo'];
        $data['sexo']=$sexo;
        $fechaNacimiento=$_POST['fechaNacimiento'];
        $data['fechaNacimiento']=$fechaNacimiento;
        $direccion=$_POST['direccion'];
        $data['direccion']=$direccion;         
        $this->model_cliente->eliminarCliente($idcliente);
        $this->load->view('head');
        $this->load->view('CrudCliente/eliminarmensaje',$data);
        //$this->load->view('footer');
    }

    public function tarifa()
    {
        $this->load->view('head');

        $this->load->view('tarifas');
        
    }    
    public function consumos()
    {
        $this->load->view('head');

        $this->load->view('consumos');
        
    }
    public function herramienta()
    {
        $this->load->view('head');

        $this->load->view('herrramientas');
        
    }
    public function calculadora()
    {
        $this->load->view('head');

        $this->load->view('calcu');
        
    }
    public function calendario()
    {
        $this->load->view('head');
        
        $this->load->view('calenda');
        
    }

}

/* End of file Controllername.php */

?>
<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index()
    {
        $this->load->view('head');
        
        $this->load->view('inicio');
        
    }
     public function inicio()
    {
        $this->load->view('head');

        $this->load->view('noticia');
        
    }
    

     public function tratamientos()
    {
        $this->load->view('head');
        $data['tratamiento']=$this->model_tratamiento->retornarTratamiento();
        $this->load->view('CrudTratamiento/tratamiento',$data);        
        
   }

    public function listatratamientosxls()
    {

      //Cargamos la librería de excel.
      $this->load->library('Excel');
      $lista=$this->model_tratamiento->retornarTratamiento();
      $lista=$lista->result();

      $this->excel->setActiveSheetIndex(0);


      $nombrehoja="lista tratamiento";
      $nombrearchivo=$nombrehoja.".xls";

      //metadatos del archivo
      $this->excel->getProperties()->setCreator("Juan Creador")
                             ->setLastModifiedBy("Juan Modificador")
                             ->setTitle("Titulo")
                             ->setSubject("Asunto")
                             ->setDescription("Documento de prueba")
                             ->setKeywords("Excel PHP")
                             ->setCategory("Categoria del documento");

        $this->excel->getActiveSheet()->setTitle($nombrehoja);
        //Contador de filas
        $contador = 1;
        
        //tamano texto
        $this->excel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);

        //fondo celdas
        $this->excel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID); 
        $this->excel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('cdcdcd'); 

        //Le aplicamos ancho las columnas.
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        //si queremos que el texto se ajuste
        //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
        //si queremos que el el tamano sea automatico
        //$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

        //Le aplicamos negrita a los títulos de la cabecera.
        $this->excel->getActiveSheet()->getStyle("A{$contador}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("C{$contador}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("D{$contador}")->getFont()->setBold(true);
        //Definimos los títulos de la cabecera.
        $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'No.');
        $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'NOMBRE');
        $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'DESCRIPCION');
        $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'PRECIO');
        //Definimos la data del cuerpo.    

        //tamano texto y bold
        $this->excel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);
        $this->excel->getActiveSheet()->getStyle()->getFont()->setBold(false);

        foreach($lista as $row){
           //Incrementamos una fila más, para ir a la siguiente.
           $contador++;
           //Informacion de las filas de la consulta.
           $this->excel->getActiveSheet()->setCellValue("A{$contador}", $contador-1);
           $this->excel->getActiveSheet()->setCellValue("B{$contador}", $row->Nombre);
           $this->excel->getActiveSheet()->setCellValue("C{$contador}", $row->Descripcion);
           $this->excel->getActiveSheet()->setCellValue("D{$contador}", $row->Precio);
        }

        //Le ponemos un nombre al archivo que se va a generar.
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$nombrearchivo.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //Hacemos una salida al navegador con el archivo Excel.
        $objWriter->save('php://output');
}

   public function listatratamientospdf()
    {
        $this->load->Library('pdf');
        $lista=$this->model_tratamiento->retornarTratamiento();
        $lista=$lista->result();
        $this->pdf=new Pdf();
        $this->pdf->AddPage();
        $this->pdf->AliasNbPages();
        $this->pdf->SetTitle("Nombre");
        $this->pdf->SetLeftMargin(15);
        $this->pdf->SetRightMargin(15);
        $this->pdf->SetFillColor(210,210,210);
        $this->pdf->SetFont('Arial','B',11);
        $this->pdf->Cell(120,10,'LISTA DE TRATAMIENTOS',0,'C',0);
        $this->pdf->Ln(10);

        foreach ($lista as $row) {
            $Nombre=$row->Nombre;
            $this->pdf->Cell(60,10,$Nombre,'TBLR',0,'L',0);
            $this->pdf->Ln(10);
        }
        $this->pdf->Output('CrudTratamiento/tratamiento.pdf','I');

    }

    public function subirarchivo()
    {
    
        $this->load->view('subirform');  
    }

    public function subir()
    {
        $config['upload_path']='./uploads';

        $config['allowed_types']='gif|jpg|png';
        $this->load->library('upload',$config);

        if(!$this->upload->do_upload())
        {
            $data['error']=$this->upload->display_errors();
        }
        else
        {
            $metadatos=$this->upload->data();
            $data['img']=$metadatos['file_name'];
        }
        
        $this->load->view('subirform',$data);
      
    }



   public function modificar()
    {
        $idTratamiento=$_POST['idTratamiento'];
        $data['tratamiento']=$this->model_tratamiento->recuperarTratamiento($idTratamiento);
        $this->load->view('head');
        $this->load->view('CrudTratamiento/modificartratamientiform',$data);
        // $this->load->view('footer');
    }

    public function modificardb()
    {
        $idTratamiento=$_POST['idTratamiento'];
        $Nombre=$_POST['Nombre'];
        $data['Nombre']=$Nombre;
        $Descripcion=$_POST['Descripcion'];
        $data['Descripcion']=$Descripcion;
        $Precio=$_POST['Precio'];
        $data['Precio']=$Precio;        
        $this->model_tratamiento->modificarTratamiento($idTratamiento,$data);
        $this->load->view('head');
        $this->load->view('CrudTratamiento/modificartratamientomensaje',$data);
        // $this->load->view('footer');
    }

    public function agregar()
    {
        $this->load->view('head');
        $this->load->view('CrudTratamiento/agregarform');
        //$this->load->view('footer');
    }

    public function agregardb()
    {
        $Nombre=$_POST['Nombre'];
        $data['Nombre']=$Nombre;
        $Descripcion=$_POST['Descripcion'];
        $data['Descripcion']=$Descripcion;
        $Precio=$_POST['Precio'];
        $data['Precio']=$Precio;

         $config = array(
         array(
                 'field' => 'Nombre',
                 'label' => 'Nombre del Tratamiento',
                 'rules' => 'required',
                 'errors' => array(
                     'required' => 'El nombre es requerido',
                 ),
             ),
         array(
                 'field' => 'Descripcion',
                 'label' => 'Descripcion del tratamiento',
               
                 'errors' => array(
                     'required' => 'La descripcion es requerido',
                   
                 ),
             ),
         array(
                 'field' => 'Precio',
                 'label' => 'Precio del tratamiento',
                
                 'errors' => array(
                     'required is_numeric(0-9)' => 'El Precio es requerido',
                    
                 ),
             ),
         );
         $this->form_validation->set_rules($config);

         if ($this->form_validation->run() == FALSE)
         {
             $this->load->view('head');
             $this->load->view('CrudTratamiento/agregarform');
         }
         else
         {
              $this->model_tratamiento->agregarTratamiento($data);
              $this->load->view('head');
              $this->load->view('CrudTratamiento/agregarmensaje',$data);
              //$this->load->view('footer');

         }

    }  



    public function eliminardb()
    {
        $idTratamiento=$_POST['idTratamiento'];
        $Nombre=$_POST['Nombre'];
        $data['Nombre']=$Nombre;
        $Descripcion=$_POST['Descripcion'];
        $data['Descripcion']=$Descripcion;
        $Precio=$_POST['Precio'];
        $data['Precio']=$Precio;        
        $this->model_tratamiento->eliminarTratamiento($idTratamiento);
        $this->load->view('head');
        $this->load->view('CrudTratamiento/eliminarmensaje',$data);
        //$this->load->view('footer');
    }

    public function pacientes()
    {
        $this->load->view('head');

        $this->load->view('paciente');
        
    }    
    public function gestionCitas()
    {
        $this->load->view('head');

        $this->load->view('citas');
        
    }
    public function historia()
    {
        $this->load->view('head');

        $this->load->view('historiaPacientes');
        
    }
    public function Odontograma()
    {
        $this->load->view('head');

        $this->load->view('odonto');
        
    }
    public function animales()
    {
        $this->load->view('head');
        
        $this->load->view('raza');
        
    }

   



}

/* End of file Controllername.php */

?>
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_cliente extends CI_Model {
public function retornarCliente()
{
    $this->db->select('*');
    $this->db->from('cliente');
    $this->db->order_by('ci ','asc');
    return $this->db->get();
}
    public function recuperarCliente($idcliente)
	{
		$this->db->select('*');
		$this->db->from('cliente');
		$this->db->where('idcliente',$idcliente);
		return $this->db->get();
	}
	// error en idTatamiento hahahha
	public function modificarCliente($idcliente,$data)
	{
		$this->db->where('idcliente',$idcliente);
		$this->db->update('cliente',$data);
	}

	public function agregarCliente($data)
	{
		$this->db->insert('cliente',$data);
	}

	public function eliminarCliente($idcliente)
	{
		$this->db->where('idcliente',$idcliente);
		$this->db->delete('cliente');
	}


}

/* End of file Model_tratamiento.php */

 ?>
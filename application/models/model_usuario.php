<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_usuario extends CI_Model {
public function retornarUsuario()
{
    $this->db->select('*');
    $this->db->from('usuario');
    $this->db->order_by('primerApellido');
    return $this->db->get();
}
    public function recuperarUsuario($idUsuario)
	{
		$this->db->select('*');
		$this->db->from('usuario');
		$this->db->where('idUsuario',$idUsuario);
		return $this->db->get();
	}
	// error en idTatamiento hahahha
	public function modificarUsuario($idUsuario,$data)
	{
		$this->db->where('idUsuario',$idUsuario);
		$this->db->update('usuario',$data);
	}

	public function agregarUsuario($data)
	{
		$this->db->insert('usuario',$data);
	}

	public function eliminarUsuario($idUsuario)
	{
		$this->db->where('idUsuario',$idUsuario);
		$this->db->delete('usuario');
	}


}

/* End of file Model_tratamiento.php */

 ?>